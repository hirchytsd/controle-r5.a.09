#!/bin/bash

# Rejoindre le cluster en tant que worker
echo "Rejoindre le cluster Swarm..."
if [ -z "$1" ]; then
  echo "Veuillez fournir l'IP du manager."
  exit 1
fi

if [ -z "$2" ]; then
  echo "Veuillez fournir les identifiants de connexion SSH."
  exit 1
fi

MANAGER_IP=$1
SSH_USER=$2
TOKEN=$(ssh $SSH_USER "docker swarm join-token -q worker")

docker swarm join --token $TOKEN $MANAGER_IP:2377
