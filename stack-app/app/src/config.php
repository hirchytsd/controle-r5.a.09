<?php

$dsn = 'pgsql:host=' . (getenv('POSTGRES_HOST') ?? 'localhost') . ';port=' . (getenv('PORT') ?? '5432') . ';dbname=' . (getenv('POSTGRES_DB') ?? 'localdb');
$username = getenv('POSTGRES_USER') ?? 'localuser';
$password = getenv('POSTGRES_PASSWORD') ?? 'localpassword';

try {
    $pdo = new PDO($dsn, $username, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}