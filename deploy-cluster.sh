#!/bin/bash

# Déploiement des services sur le cluster Swarm
echo "Déploiement des stacks sur le cluster Swarm..."

docker network create --driver overlay external

# Déploiement de Traefik
docker stack deploy -c ./stack-treafik/docker-compose.yml traefik

# Déploiement des logs
docker stack deploy -c ./stack-logs/docker-compose.yml logs

# Déploiement du monitoring
docker stack deploy -c ./stack-monitoring/docker-compose.yml monitoring

# Déploiement de l'application (prod et dev)
docker stack deploy -c ./stack-app/docker-compose.yml app

echo "Déploiement terminé."
