#!/bin/bash

echo "Déploiement sur la machine locale..."

docker network create external

docker-compose -f ./stack-treafik/docker-compose.yml up -d
docker-compose -f ./stack-logs/docker-compose.yml up -d
docker-compose -f ./stack-monitoring/docker-compose.yml up -d
docker-compose -f ./stack-app/docker-compose.yml up -d

echo "Déploiement terminé."
