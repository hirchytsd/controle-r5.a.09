#!/bin/bash

if [ -z "$1" ]; then
  echo "Veuillez fournir l'IP du manager."
  exit 1
fi

MANAGER_IP=$1

# Initialisation du manager Swarm
echo "Initialisation du manager Swarm..."
docker swarm init --advertise-addr $MANAGER_IP

# Afficher la commande pour ajouter les nodes
echo "Utilisez la commande suivante sur chaque node pour les joindre au cluster :"
docker swarm join-token worker
