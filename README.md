# TP noté swarm

Groupe 2 :
- Daniil Hirchyts
- Giovanni Gozzo
- Marius Brouty
- Clément Garro

# Scripts shell

Comme nous ne pouvions pas faire tourner swarm sur nos mac, nous avons utilisé Docker Playground pour faire le tp. 
Voici comment utiliser les différents scripts.

## init-manager.sh

Ce script permet de démarrer le manager. Il faut l'utiliser comme ceci :

```bash
./init-manager.sh 192.168.0.x
```

Il faut spécifier l'ip local sur lequel on veut lancer swarm.

## init-node.sh

Ce script permet de démarrer un noeud. Il faut l'utiliser comme ceci :

```bash
./init-node.sh 192.168.0.x ip172-18-0-84-crpvraa91nsg00c6h...
```

On passe en paramètre en premier l'ip du manager puis les identifiants de connexion ssh du manager.
Celle-ci sont fourni par Docker Playground, il faut copier le champs `ssh` sur le noeud manager.
Le script récupère le token via ssh puis se connecte au swarm.

## deploy.sh

Ce script permet de lancer tous les docker-compose.yml en local sur la machine.

```bash
./deploy.sh
```

## deploy-cluster.sh

Ce script lance les stacks sur le cluster swarm.

```bash
./deploy-cluser.sh
```